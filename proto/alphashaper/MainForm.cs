﻿using MapSurfer;
using MapSurfer.Geometries;
using MapSurfer.IO.FileTypes;
using MapSurfer.Reflection;
using MapSurfer.Rendering;
using MapSurfer.Utilities;
using MapSurfer.Windows.Forms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace alphashaper
{
    public partial class MainForm : Form
    {
        private DBManager dbman;
        private MapViewer m_mapViewer;
        private Renderer m_renderer;
        private SharpMap.Layers.VectorLayer vlay;
        private SharpMap.Layers.VectorLayer pointLayer;
        private SharpMap.Layers.VectorLayer alphaLayer;
        private SharpMap.Styles.VectorStyle landStyle;
        private SharpMap.Styles.VectorStyle waterStyle;

        public MainForm()
        {
            InitializeComponent();
            dbman = new DBManager();
            dbman.connect("Host=192.168.1.149;Username=postgres;Password=isochrone;Database=routing");

            vlay = new SharpMap.Layers.VectorLayer("CPT");
            pointLayer = new SharpMap.Layers.VectorLayer("POINTS");
            alphaLayer = new SharpMap.Layers.VectorLayer("AlphaLayer");

            landStyle = new SharpMap.Styles.VectorStyle();
            landStyle.Fill = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(232,232,232));

            waterStyle = new SharpMap.Styles.VectorStyle();
            waterStyle.Fill = new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(198, 198, 255));

            Dictionary<string, SharpMap.Styles.IStyle> styles = new Dictionary<string, SharpMap.Styles.IStyle>();
            styles.Add("land", landStyle);
            styles.Add("water", waterStyle);

            //vlay.Theme = new SharpMap.Rendering.Thematics.UniqueValuesTheme<string>("class", styles, landStyle);
            ProjNet.CoordinateSystems.Transformations.CoordinateTransformationFactory ctFact = new ProjNet.CoordinateSystems.Transformations.CoordinateTransformationFactory();
            vlay.CoordinateTransformation = ctFact.CreateFromCoordinateSystems(ProjNet.CoordinateSystems.GeographicCoordinateSystem.WGS84, ProjNet.CoordinateSystems.ProjectedCoordinateSystem.WebMercator);
            vlay.ReverseCoordinateTransformation = ctFact.CreateFromCoordinateSystems(ProjNet.CoordinateSystems.ProjectedCoordinateSystem.WebMercator, ProjNet.CoordinateSystems.GeographicCoordinateSystem.WGS84);

            pointLayer.CoordinateTransformation = ctFact.CreateFromCoordinateSystems(ProjNet.CoordinateSystems.GeographicCoordinateSystem.WGS84, ProjNet.CoordinateSystems.ProjectedCoordinateSystem.WebMercator);
            pointLayer.ReverseCoordinateTransformation = ctFact.CreateFromCoordinateSystems(ProjNet.CoordinateSystems.ProjectedCoordinateSystem.WebMercator, ProjNet.CoordinateSystems.GeographicCoordinateSystem.WGS84);

            alphaLayer.CoordinateTransformation = ctFact.CreateFromCoordinateSystems(ProjNet.CoordinateSystems.GeographicCoordinateSystem.WGS84, ProjNet.CoordinateSystems.ProjectedCoordinateSystem.WebMercator);
            alphaLayer.ReverseCoordinateTransformation = ctFact.CreateFromCoordinateSystems(ProjNet.CoordinateSystems.ProjectedCoordinateSystem.WebMercator, ProjNet.CoordinateSystems.GeographicCoordinateSystem.WGS84);

            mapBox1.Map.BackgroundLayer.Add(new SharpMap.Layers.TileAsyncLayer(new BruTile.Web.OsmTileSource(), "OSM"));
            mapBox1.Map.Layers.Add(pointLayer);
            mapBox1.Map.Layers.Add(vlay);
            mapBox1.Map.Layers.Add(alphaLayer);
            mapBox1.Map.ZoomToExtents();
            mapBox1.Refresh();
            mapBox1.ActiveTool = SharpMap.Forms.MapBox.Tools.Pan;

            Console.Write("testing it out");
            button1_Click(null, null);
        }

        private void LoadProject(string fileName)
        {
            Map map = Map.FromFile(fileName);
            map.Initialize(Path.GetDirectoryName(fileName), true);
            m_mapViewer.Map = map;
            m_mapViewer.ZoomToFullExtent();
        }

        private void mapBox1_Click(object sender, System.EventArgs e)
        {

        }


        //Load the lines from the DB and then plot them. Only for the on line geom though.
        private async void button1_Click(object sender, System.EventArgs e)
        {
            dbman.Table = "cpt_2po_4pgr";

            int startPointValue = Convert.ToInt32(startingPoint.Text);
            int costValue = Convert.ToInt32(totalCost.Text);
            string alpha = alphaValue.Text;
            await dbman.IsoChrone(startPointValue, costValue);
            await dbman.AlphaShape(alpha);

            List<Point> polyList = dbman.PolyPoints;
            List<Point> pointList = dbman.Points;
            List<Line> lines = dbman.Lines;
            
            DrawOnMapFromLines(lines);
            DrawOnMapFromPoints(pointList);
            DrawOnMapFromPolygon(polyList);
        }

        private void DrawOnMapFromLines(List<Line> lines)
        {
            Collection<GeoAPI.Geometries.IGeometry> geomColl = new Collection<GeoAPI.Geometries.IGeometry>();

            GeoAPI.GeometryServiceProvider.Instance = new NetTopologySuite.NtsGeometryServices();
            GeoAPI.Geometries.IGeometryFactory gf = GeoAPI.GeometryServiceProvider.Instance.CreateGeometryFactory();// .DefaultGeometryFactory;
            
            for(int i =0; i < lines.Count; i++)
            {
                List<GeoAPI.Geometries.Coordinate> pointList = new List<GeoAPI.Geometries.Coordinate>();
                for(int j = 0; j < lines[i].PointList.Count; j++)
                {
                    pointList.Add(new GeoAPI.Geometries.Coordinate( lines[i].PointList[j].Latitude, lines[i].PointList[j].Longitude ));
                }

                GeoAPI.Geometries.Coordinate[] points = pointList.ToArray();
                
                geomColl.Add(gf.CreateLineString(points));
            }

            vlay.DataSource = new SharpMap.Data.Providers.GeometryProvider(geomColl);
            //mapBox1.Map.ZoomToExtents();
            mapBox1.Refresh();
        }

        private void DrawOnMapFromPoints(List<Point> points)
        {
            Collection<GeoAPI.Geometries.IGeometry> geomColl = new Collection<GeoAPI.Geometries.IGeometry>();
            GeoAPI.GeometryServiceProvider.Instance = new NetTopologySuite.NtsGeometryServices();
            GeoAPI.Geometries.IGeometryFactory gf = GeoAPI.GeometryServiceProvider.Instance.CreateGeometryFactory();// .DefaultGeometryFactory;

            for(int i = 0; i < points.Count; i++)
            {
                geomColl.Add(gf.CreatePoint(new GeoAPI.Geometries.Coordinate(points[i].Latitude, points[i].Longitude)));
            }
            
            pointLayer.DataSource = new SharpMap.Data.Providers.GeometryProvider(geomColl);

            //mapBox1.Map.ZoomToExtents();
            mapBox1.Refresh();
        }

        private void DrawOnMapFromPolygon(List<Point> points)
        {
            Collection<GeoAPI.Geometries.IGeometry> geomColl = new Collection<GeoAPI.Geometries.IGeometry>();
            GeoAPI.GeometryServiceProvider.Instance = new NetTopologySuite.NtsGeometryServices();
            GeoAPI.Geometries.IGeometryFactory gf = GeoAPI.GeometryServiceProvider.Instance.CreateGeometryFactory();// .DefaultGeometryFactory;

            List<GeoAPI.Geometries.Coordinate> coordinates = new List<GeoAPI.Geometries.Coordinate>();
            for (int i = 0; i < points.Count; i++)
            {
                GeoAPI.Geometries.Coordinate coord = new GeoAPI.Geometries.Coordinate(points[i].Latitude, points[i].Longitude);
                coordinates.Add(coord);
            }

            geomColl.Add(gf.CreatePolygon(coordinates.ToArray()));

            alphaLayer.DataSource = new SharpMap.Data.Providers.GeometryProvider(geomColl);

            //mapBox1.Map.ZoomToExtents();
            mapBox1.Refresh();
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
