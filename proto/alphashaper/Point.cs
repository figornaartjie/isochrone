﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alphashaper
{
    class Point
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }

        public static List<Point> PointListFromPostGisLineString(string postGisString)
        {
            string[] coords = postGisString.Split('(').Last().Split(')').First().Split(',');

            List<Point> pointList = new List<Point>();

            for (int i = 0; i < coords.Length; i++)
            {
                Point point = new Point();
                point.Latitude = Double.Parse(coords[i].Split(' ')[0].Trim());
                point.Longitude = Double.Parse(coords[i].Split(' ')[1].Trim());
                pointList.Add(point);
            }

            return pointList;
        }

        public static List<Point> PointListFromPostGisLineString(object postGisString)
        {
            return PointListFromPostGisLineString(Convert.ToString(postGisString));
        }

        public override string ToString()
        {
            return "[" + Longitude + "," + Latitude + "]";
        }

        internal static Point PointFromPointSring(string pointString)
        {
            Point point = new Point();

            string[] pts = pointString.Split('(').Last().Split(')').First().Split(' ');

            point.Latitude = Convert.ToDouble(pts[0].Trim());
            point.Longitude = Convert.ToDouble(pts[1].Trim());

            Console.WriteLine(pointString);
            return point;
        }

        internal static Point PointFromPointSring(object pointString)
        {
            return PointFromPointSring(Convert.ToString(pointString));
        }
    }
}
