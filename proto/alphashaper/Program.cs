﻿using MapSurfer.Samples;
using MapSurfer.Utilities;

using Npgsql;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//using System.Windows.Forms.

namespace alphashaper
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string version = MSNUtility.TryDetectInstalledVersion();  // or one can define version manually MSNUtility.SetCurrentMSNVersion(..)
            MSNUtility.SetCurrentMSNVersion(version);
            AssemblyLoader.AddSearchPath(Path.Combine(MSNUtility.GetMSNInstallPath(), "Studio"));
            AssemblyLoader.Register(AppDomain.CurrentDomain, version);

            Application.Run(new MainForm());

            using (var conn = new NpgsqlConnection("Host=192.168.1.149;Username=postgres;Password=isochrone;Database=routing"))
            {
                conn.Open();
                
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;

                    //cmd.CommandText = "SELECT seq, node, edge, cost, geom::TEXT FROM isowalkdissel";
                    cmd.CommandText = "SELECT seq, id1 AS node, id2 AS edge, geom::TEXT, ST_AsTEXT(geom) FROM pgr_drivingdistance('select id, source, target, length_m as cost from cpt_2po_4pgr', 1970, 350, false,false) as di JOIN walkingnodes_new wn on di.id1 = wn.id";
                    //cmd.UnknownResultTypeList = new[] { false, false, false, true };
                    //cmd.AllResultTypesAreUnknown = true;

                    using (NpgsqlDataReader reader = cmd.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            Console.Write(" | ");
                            
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                switch(reader.GetDataTypeName(i))
                                {
                                    case "int4":
                                        Console.Write(reader.GetString(i) + " | ");
                                        break;
                                    case "float8":
                                        Console.Write(reader.GetFieldValue<double>(i) + " | ");
                                        break;
                                    default:
                                        Console.Write(reader.GetFieldValue<string>(i) + " | ");
                                        //var tempCmpd = new NpgsqlCommand();
                                        //tempCmpd.Connection = conn;
                                        //tempCmpd.CommandText = "SELECT ST_AsText('" + reader.GetFieldValue<string>(i) + "')";
                                        //var tempReader = tempCmpd.ExecuteReaderAsync();
                                        //while(tempReader.)
                                        //{
                                        //    Console.Write(reader.GetString(0));
                                        //}
                                        break;
                                }
                            }
                            Console.WriteLine("\n\r");
                        }
                    }
                }

                Console.ReadLine();
            }
        }
    }
}
