﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alphashaper
{
    class IsochroneResultRow
    {
        public int Seq { get; set; }
        public int Node { get; set; }
        public int Edge { get; set; }

        public double Cost { get; set; }
        public string PointString { get; set; }
        public string LineString { get; set; }
    }
}
