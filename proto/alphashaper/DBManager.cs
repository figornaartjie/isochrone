﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alphashaper
{
    class DBManager
    {
        NpgsqlConnection conn;
        string BaseQuery { get; set; }
        public string Table { get; set; }
        public string AlphaPolygon { get; private set; }
        public List<Point> Points { get; private set; }
        public List<Line> Lines { get; private set; }
        public List<IsochroneResultRow> IsoList { get; private set; }
        public List<string> Geometries { get; private set; }
        public List<Point> PolyPoints { get; private set; }

        public DBManager()
        {
            Points = new List<Point>();
            Lines = new List<Line>();
            IsoList = new List<IsochroneResultRow>();
            Geometries = new List<string>();
        }

        public bool connect(string connectionString)
        {
            conn = new NpgsqlConnection(connectionString);

            conn.Open();
            if (conn.State == System.Data.ConnectionState.Open)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<List<string>> ReadRoads(List<int> roads)
        {
            List<string> results = new List<string>();
            if (roads.Count == 0)
            {
                return results;
            }
            var command = new NpgsqlCommand();
            command.Connection = this.conn;

            string sqlCommand = "select ST_AsTEXT(s.geom_way) from " + this.Table + " s WHERE s.source = " + roads[0];

            for (int i = 1; i < roads.Count; i++)
            {
                sqlCommand += " or s.source = " + roads[i];
            }

            command.CommandText = sqlCommand;

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    results.Add(reader.GetFieldValue<string>(0));
                }
                return results;
            }
        }

        public async Task<List<IsochroneResultRow>> IsoChrone(int startSrc, int distance)
        {
            var command = new NpgsqlCommand();
            command.Connection = this.conn;
            BaseQuery =
                "SELECT seq, id1 AS node, id2 AS edge, di.cost as cost, ST_AsText(geom) as geom, ST_AsText(geom_way) as geom_way " +
                " FROM pgr_drivingdistance('select id, source, target, length_m as cost from cpt_2po_4pgr', " + startSrc + ", " + distance + ", false,false) " +
                "as di JOIN walkingnodes_new wn on di.id1 = wn.id JOIN cpt_2po_4pgr cpt on wn.id = cpt.id";

            command.CommandText = BaseQuery;

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    IsochroneResultRow irr = new IsochroneResultRow();
                    irr.Seq = Convert.ToInt32(reader[0]);
                    irr.Node = Convert.ToInt32(reader[1]);
                    irr.Edge = Convert.ToInt32(reader[2]);
                    irr.Cost = Convert.ToDouble(reader[3]);
                    irr.PointString = Convert.ToString(reader[4]);
                    irr.LineString = Convert.ToString(reader[5]);

                    IsoList.Add(irr);

                    var line = new Line(Point.PointListFromPostGisLineString(irr.LineString));
                    Lines.Add(line);

                    var point = Point.PointFromPointSring(irr.PointString);
                    Points.Add(point);

                    Geometries.Add(irr.PointString);
                }
                return IsoList;
            }
        }

        public async Task<List<Point>> AlphaShape(string alphaValue)
        {
            List<Point> points;// = new List<Point>();
            var command = new NpgsqlCommand();
            command.Connection = this.conn;
            command.CommandText =
                "SELECT ST_ASTEXT(ST_ConcaveHull(ST_UNION(geom), " + alphaValue + ")) from (" + BaseQuery + ") as rr";

            using (var reader = command.ExecuteReader())
            {
                reader.Read();
                AlphaPolygon = Convert.ToString(reader[0]);

                return points = PolyPoints = Point.PointListFromPostGisLineString(reader[0]);
            }
        }
    }
}
