﻿namespace alphashaper
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mapBox1 = new SharpMap.Forms.MapBox();
            this.loadShapeButton = new System.Windows.Forms.Button();
            this.startingPoint = new System.Windows.Forms.TextBox();
            this.totalCost = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.alphaValue = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mapBox1
            // 
            this.mapBox1.ActiveTool = SharpMap.Forms.MapBox.Tools.None;
            this.mapBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.mapBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapBox1.FineZoomFactor = 10D;
            this.mapBox1.Location = new System.Drawing.Point(0, 0);
            this.mapBox1.MapQueryMode = SharpMap.Forms.MapBox.MapQueryType.LayerByIndex;
            this.mapBox1.Name = "mapBox1";
            this.mapBox1.QueryGrowFactor = 5F;
            this.mapBox1.QueryLayerIndex = 0;
            this.mapBox1.SelectionBackColor = System.Drawing.Color.White;
            this.mapBox1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.mapBox1.ShowProgressUpdate = false;
            this.mapBox1.Size = new System.Drawing.Size(1025, 551);
            this.mapBox1.TabIndex = 0;
            this.mapBox1.Text = "mapBox1";
            this.mapBox1.WheelZoomMagnitude = -2D;
            this.mapBox1.Click += new System.EventHandler(this.mapBox1_Click);
            // 
            // loadShapeButton
            // 
            this.loadShapeButton.Location = new System.Drawing.Point(15, 169);
            this.loadShapeButton.Name = "loadShapeButton";
            this.loadShapeButton.Size = new System.Drawing.Size(75, 23);
            this.loadShapeButton.TabIndex = 1;
            this.loadShapeButton.Text = "Load Shape";
            this.loadShapeButton.UseVisualStyleBackColor = true;
            this.loadShapeButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // startingPoint
            // 
            this.startingPoint.Location = new System.Drawing.Point(12, 43);
            this.startingPoint.Name = "startingPoint";
            this.startingPoint.Size = new System.Drawing.Size(131, 20);
            this.startingPoint.TabIndex = 2;
            this.startingPoint.Text = "1970";
            // 
            // totalCost
            // 
            this.totalCost.Location = new System.Drawing.Point(12, 88);
            this.totalCost.Name = "totalCost";
            this.totalCost.Size = new System.Drawing.Size(131, 20);
            this.totalCost.TabIndex = 3;
            this.totalCost.Text = "350";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(12, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Cost";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(12, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Map Starting Point";
            // 
            // alphaValue
            // 
            this.alphaValue.Location = new System.Drawing.Point(12, 137);
            this.alphaValue.Name = "alphaValue";
            this.alphaValue.Size = new System.Drawing.Size(131, 20);
            this.alphaValue.TabIndex = 6;
            this.alphaValue.Text = "0.99";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(12, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Alpha Value";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 551);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.alphaValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.totalCost);
            this.Controls.Add(this.startingPoint);
            this.Controls.Add(this.loadShapeButton);
            this.Controls.Add(this.mapBox1);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuOpenProject;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mnuExit;
        private System.Windows.Forms.ToolStripMenuItem mnuZoomToExtent;
        private System.Windows.Forms.ToolStripMenuItem zoomToExtentToolStripMenuItem;
        private SharpMap.Forms.MapBox mapBox1;
        private System.Windows.Forms.Button loadShapeButton;
        private System.Windows.Forms.TextBox startingPoint;
        private System.Windows.Forms.TextBox totalCost;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox alphaValue;
        private System.Windows.Forms.Label label4;
    }
}